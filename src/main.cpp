//!
//! \file main.cpp
//! \brief Main driver
//! \author Joshua Thames
//! \date 2020-04-24
//! 
//! \copyright Copyright (c) Joshua Thames 2020
//!

/*! \mainpage CppAutomation and Exploration!
 *
 * \section intro_sec Introduction
 *
 * This is the introduction.
 *
 * \section install_sec Installation
 *
 * \subsection step1 Step 1: Open the box
 *  
 * etc...
 */

#include "mymath.h"
#include <fstream>
#include <iostream>
#include <vector>
#include <QApplication>
#include <QPushButton>

#define CATCH_CONFIG_RUNNER
#include "catch2/catch.hpp"

//!
//!\brief Main file to demo c++ automation techniques.
//!
//!\param argc 
//!\param argv 
//!\return int 
//!
int main(int argc, char *argv[])
{
     QApplication app (argc, argv);

    QPushButton button ("Hello world !");
    button.show();

    std::vector<int> vec = {1, 2, 3, 4, 5};
    Math mymath;

    int valSum = mymath.sum(vec);
    unsigned int valFactorial = mymath.factorial(valSum);

    std::ofstream testOutput("test1.txt", std::ofstream::out);
    testOutput << valSum << std::endl;
    testOutput << valFactorial << std::endl;

    Catch::Session session; // There must be exactly one instance

    int height = 0; // Some user variable you want to be able to set

    // Build a new parser on top of Catch's
    using namespace Catch::clara;
    // clang-format off
    auto cli = session.cli()     // Get Catch's composite command line parser
         | Opt(height, "height") // bind variable to a new option, with a hint string
              ["-g"]["--height"] // the option names it will respond to
              ("how high?");     // description string for the help output
    // clang-format on

    // Now pass the new composite back to Catch so it uses that
    session.cli(cli);

    // Let Catch (using Clara) parse the command line
    int returnCode = session.applyCommandLine(argc, argv);
    if (returnCode != 0) // Indicates a command line error
        return returnCode;

    // if set on the command line then 'height' is now set at this point
    if (height > 0) {
        std::cout << "height: " << height << std::endl;
    }

    // session.run();

    return app.exec();
}