//!
//! \file test.cpp
//! \brief 
//! \author Joshua Thames
//! \date 2020-04-24
//! 
//! \copyright Copyright (c) Joshua Thames 2020
//!

#include "catch2/catch.hpp"
#include "mymath.h"

//!
//! \brief Basic Test
//!
TEST_CASE("My Math class", "[require]")
{
    Math mymath;

    std::vector<int> vec = {1, 1};

    CHECK(2 == mymath.sum(vec));
}