//!
//! \file mymath.h
//! \brief Dummy math class
//! \author Joshua Thames
//! \date 2020-04-24
//! 
//! \copyright Copyright (c) Joshua Thames 2020
//!

#include <vector>

//!
//! \brief Dummy Math class
//! \class Math
//!
class Math {
  public:

    //!
    //! \brief Sum for vector contents
    //!
    //! \param vec 
    //! \return int 
    //!
    auto sum(std::vector<int> &vec) -> int
    {
        int val = 0;
        for (auto &i : vec) {
            val += i;
        }
        return val;
    }

    //!
    //! \brief Computes the factorial of a given number.
    //! 
    //! \param number 
    //! \return unsigned int 
    //!
    unsigned int factorial(unsigned int number)
    {
        return number <= 1 ? number : factorial(number - 1) * number;
    }
};